# Portfolio 1: Ransomware Protection

## Table of Contents

[[_TOC_]]

## Background

The goal of this assignment is for you to learn about how you can protect
against ransomware. Ransomware is one of the biggest problems in cybersecurity
and it will not go away since the incentives (mostly "easy money") are strong.
[The only real protection against
ransomware](https://redmondmag.com/articles/2019/10/22/how-to-ransomware-proof-your-backups.aspx))
is backup/restore. Backup/restore is a core service in an infrastructure. Few
things hurt more than loosing valuable data. Being able to do restore is what
will save you if you are the victim of ransomware, but there also [other things
you can do to prevent becoming the
victim](https://docs.microsoft.com/en-us/windows/security/threat-protection/microsoft-defender-atp/controlled-folders).

To do this assignment, you need to know

* Storage/backup/restore theory (from DCS{T,G}1001)
* Windows
* PowerShell
* An editor (vscode)
* git
* Markdown

## What should be handed in in Blackboard?

A PDF-document _generated from the README.md in your git-repo_ with 1200 (+/-
10%) words describing your proposed solution.

* **The PDF must contain a link to your PRIVATE git-repo where Tor Ivar and Erik
  have been added as developers**.

All students (both in Trondheim and Gjøvik) should add both Tor Ivar and Erik to
their git-repos (our NTNU usernames are `melling` and `erikhje`) You can
generate the PDF by simply printing from your browser and choose "Save as PDF".

<!--    * **The PDF must contain a link to a video in Stream (only Tor Ivar and Erik should have access)**
-->

## Rules

* This is an individual assignment, but you are allowed to discuss with other
  students.
* If you have received significant input/ideas from others, you must mention it
  in the report
* [No cheating](https://innsida.ntnu.no/wiki/-/wiki/English/Cheating+on+exams)
  (we will check for plagiarism)

## The Assignment

* Write a ransomware-protection script in PowerShell (should be between 10 and
  250 lines of code)
  * _Start small, find our what you would like to copy and where to copy to, and
    then write commands for doing it, and put them in a script_
  * Consider your design choices, e.g. local backup, remote/cloud backup,
    encrypted, compression, which files to include, backup schedule, restore
    functionality, etc (see lecture notes about backup/restore)
  * Feel free to add functionality for other system configuration changes that
    will protect you
  * The script does not have to work, meaning if you write something to use a
    paid backup service or similar, then of course we dont expect you to include
    credentials for external accounts
  * The setting the script should work in can be either your lab infrastructure,
    your personal laptop or some other Windows-based host/infrastructure
    <!--* Demo the script and an example of a file restore in a video you upload to Stream (max two minutes)-->
* Write your individual report in the file README.md (using
  [markdown](https://docs.gitlab.com/ee/user/markdown.html)) where you 
  * justify/explain your design
  * use links to cite your sources
  * briefly discuss the security of your script
  * reflect over your solution and work process

See [README.md](./README.md) for a brief template you can start with if you want to.

## Grading and feedback

* You will be graded based on
  * PowerShell skills
  * Report: language, use of sources, justification, technology understanding,
    reflection
  * X-factor? (a typical good solution is a C, a very good solution is a B, to
    get an A ("outstanding") you need to impress and show us an X-factor)
  * You will receive an "approximate grade" (as explained in the course
    description and in the lectures) in Blackboard and a paragraph justifying
    your grade ("automatisk begrunnelse")